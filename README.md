# SofaTools python package : ease the creation of a scene

See [the documentation](https://sofatools.readthedocs.io/en/latest/) on readthedocs.

+ [Install](#install)
+ [Sofa scene launcher](#sofa-scene-launcher)
+ [VTK unstructured grid reader](#vtk-unstructured-grid-reader)
+ [Display the graph of a scene](#display-the-graph-of-a-scene)

## Install

```python
pip install sofatools
# or python3 -m pip install --user sofatools
```

## Sofa scene launcher
```python
from sofatools import Launcher
import os

curDir = os.getcwd()

fmm = Launcher({
    "SOFA_PATH" : "path_to_sofa_build/bin/runSofa",
    "SCENE_PATH" : os.path.join(curDir, "scene.py"),
    "RESULT_DIR_PATH" : os.path.join(curDir, "Output"),
})

fmm["N_IT"] = 10
fmm.run()
```

Here are the implemented parameters :

  | Key | Value |
  |:---:|:-----:|
  | `SOFA_PATH` | Path of runSofa : `path_to_sofa/bin/runSofa` |
  | `SCENE_PATH` | Path of the python scene |
  | `G_MODE` | `batch` or `qglviewer` (default `batch`) |
  | `RESULT_DIR_PATH` | Directory for output and log, will be created if doesn't exist |
  | `OUT_TO_FILE` | Write output in files or in terminal (default `False`) |
  | `STDOUT_PATH` | Path for standard output file (default `RESULT_DIR_PATH`/stdout.out) |
  | `STDERR_PATH` | Path for standard errors file (default `RESULT_DIR_PATH`/stdout.out) |
  | `DT` | Time step of the simulation (default `0.01`) |
  | `GRAVITY` | Gravity (default `"0 0 0"`) |
  | `N_IT` | Number of time iterations (default `100`) |

To launch the scene, run
```python
python3 launch.py
```



## VTK unstructured grid reader

```python
from sofatools import MeshReader

m = MeshReader("mesh.vtk")
print(m)
# ** Class vtk unstructured grid reader **
# Filename : path_to_mesh.vtk
# Points : 189
# Cells : 1608
# Tetras : 480
# Triangles : 1128
# Vertices : 0
# Lines : 0
# Hexas : 0


m.positions[2] # Position of point 2
# (3.2, 5.932, 0.0)

m.tetras[3]
# ** Class Cell **
# Type : Tetrahedron (10)
# Id : 3
# Points : 50 12 150 51

m.tetras[3][0] # Id of the first point in tetrahedron 3
# 50

m.cells[0] # return cell 0
for cell in m.cells # iterate over the cells

m.positions[m.triangles[3][0]] # Position of the first point in triangle 3
m.triangles[3].id # Id in cells of the third triangle
# 483
m.triangles[3].points # List of points of the triangle
# [4, 12, 50]

data = m.getCellData("name")
for d in data # iterate over the data
data.size
# 150
data.range
# (0.0, 15.0)
data[3] # Get the value on the third cell
# 12
```





## Display the graph of a scene

```python
from sofatools import Graph

print(Graph("scene.py"))
```

If you add it to your `bashrc` :
```
affScene scene.py >> graph.txt
```

Example output :

    rootNode
         VisualStyle
         DefaultAnimationLoop
         DefaultVisualManagerLoop
         RequiredPlugin
         RequiredPlugin
         EulerImplicitSolver
         CGLinearSolver
         -> Ellipsoid
              ZonesVTKLoader
              Mesh
              ZoneFMM
              PlaceOrientDOFs
              MechanicalObject
              ZoneShapeFunction
              MechanicalObject
              BarycentricShapeFunction
              -> behavior
                   ZoneGaussPointSampler
                   TopologyGaussPointSampler
                   MechanicalObject
                   MLSMapping
                   -> E
                        MechanicalObject
                        GreenStrainMapping
                        HookeForceField
              -> visual
                   MeshSTLLoader
                   OglModel
                   MLSMapping
