import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sofatools",
    version="0.3.002",
    author="Gaetan Desrues",
    author_email="gaetan.desrues@inria.fr",
    description="Python package to ease the use of the Sofa-Framework and the creation of a scene.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.inria.fr/gdesrues1/sofatools",
    packages=setuptools.find_packages(),
    install_requires=['vtk==8.1.2','numpy==1.17.4'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
