from sofatools import Launcher
import os


curDir = os.getcwd()

launcher = Launcher({
    "SOFA_PATH" : "path_to_SOFA_build/bin/runSofa",
    "SCENE_PATH" : os.path.join(curDir, "scene", "scene.py"),
    "RESULT_DIR_PATH" : os.path.join(curDir, "Res"),
    "MESH_STL" : os.path.join(curDir, "mesh", "mesh.stl"),
    "MESH_VKT" : os.path.join(curDir, "mesh", "mesh.vtk"),
    "PRESSURE" : -2000,
    "N_IT" : 200,
    # "G_MODE" : "qglviewer",
})

launcher.setResDir()

launcher.setParams({
    "POISSON_RATIO" : 0.3,
    "YOUNG_MODULUS" : 15e6,
    "MESH_DEFORMED" : os.path.join(curDir, launcher["RESULT_DIR_PATH"], "deformed.vtk")
})
launcher.run()
























#
