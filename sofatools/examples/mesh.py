from os.path import join, dirname, abspath
from sofatools import Mesh
import numpy as np

mesh_filename = join(dirname(abspath(__file__)), 'mesh', 'mesh.vtk')
m = Mesh(mesh_filename)
print(m)

### Point position
print("Coordinates of point 113:")
print(m.positions[113])

### Cell of id 0
cell = m.cells[34]
print(cell)


### Point getitem
print(' '.join(str(p.id) for p in cell))
# print(cell[0]) # --> cell[0] returns point 0
### Point generator
print(' '.join(str(p.id) for p in cell.points))
# print(cell.points[0]) # --> cell.points is a generator

### Cell center
print(f"Cell center: {cell.getCenter()}")


### CellDataArray
print("\nDataArray of name 'Zones'")
zones = m.getCellData("Zones")
print("Range: "+str(zones.range))
print("Value of data of id 4: "+str(zones[4]))

### New DataArray in tetras
data = np.zeros(m.nbCells)
for i,cell in enumerate(m.getCells(10)):
    data[cell.id] = i
m.addCellData(data, 'NewData')
m.write(join(dirname(abspath(__file__)), 'test.vtk'))
