import os
import sys
import json
from subprocess import call

class Launcher:
    """ Provides an easy to use way to launch SOFA scenes. Generates an output
        directory and a parameters file using json.

        :param dict dic: Dictonary with parameters

        .. table:: Default parameters

            +-----------------+----------------------------------------------------------------------+
            |       Key       |                                 Value                                |
            +=================+======================================================================+
            |   `SOFA_PATH`   |          Path of runSofa : `path_to_sofa/bin/runSofa`                |
            +-----------------+----------------------------------------------------------------------+
            |   `SCENE_PATH`  |                    Path of the python scene                          |
            +-----------------+----------------------------------------------------------------------+
            |     `G_MODE`    |            `batch` or `qglviewer` (default `batch`)                  |
            +-----------------+----------------------------------------------------------------------+
            |`RESULT_DIR_PATH`| Directory for output and log, will be created if doesn't exist       |
            +-----------------+----------------------------------------------------------------------+
            |  `OUT_TO_FILE`  |      Write output in files or in terminal (default `False`)          |
            +-----------------+----------------------------------------------------------------------+
            |  `STDOUT_PATH`  | Path for standard output file (default `RESULT_DIR_PATH`/stdout.out) |
            +-----------------+----------------------------------------------------------------------+
            |  `STDERR_PATH`  | Path for standard errors file (default `RESULT_DIR_PATH`/stderr.out) |
            +-----------------+----------------------------------------------------------------------+
            |       `DT`      |          Time step of the simulation (default `0.01`)                |
            +-----------------+----------------------------------------------------------------------+
            |    `GRAVITY`    |                   Gravity (default `"0 0 0"`)                        |
            +-----------------+----------------------------------------------------------------------+
            |      `N_IT`     |            Number of time iterations (default `100`)                 |
            +-----------------+----------------------------------------------------------------------+

    """
    def __init__(self, dic=dict()):
        self.params = {
            "SOFA_PATH" : None,
            "G_MODE" : "batch",#qglviewer
            "SCENE_PATH" : None,
            "DT" : 0.01,
            "GRAVITY" : "0 0 0",
            "N_IT" : 100,
            "RESULT_DIR_PATH" : None,
            "OUT_TO_FILE" : False,
            "STDOUT_PATH" : None,
            "STDERR_PATH" : None,
        }
        self.setParams(dic)


    def __getitem__(self, key):
        """ Accessor to a parameter

            :param str key: Key of the parameter in the dictionary

            :returns: Value of the parameter
        """
        return self.params[key]


    def __setitem__(self, key, value):
        """ Sets a item in the parameters dictionary

            :param str key: Key of the parameter in the dictionary
            :param value: Value of the parameter in the dictionary
        """
        self.params[key] = value


    def checkRun(self):
        """ This method is called before running the scene. It checks that all
            necessary parameters have been specified

            .. note:: It will create the output directory if it doesn't exist
        """
        if self['SOFA_PATH'] is None:
            raise RuntimeError("Specify the path of sofa executable runSofa with SOFA_PATH")
        elif not os.path.exists(self['SOFA_PATH']):
            print("{}: Executable does not exist\nSpecify the path of sofa executable runSofa with SOFA_PATH".format(self['SOFA_PATH']))
            sys.exit()
        if self["SCENE_PATH"] is None:
            raise RuntimeError("Specify a path for the python sofa scene with SCENE_PATH")
        if self["RESULT_DIR_PATH"] is None:
            raise RuntimeError("Specify a prefix directory for the output with RESULT_DIR_PATH")
        if not os.path.exists(self["RESULT_DIR_PATH"]):
            os.mkdir(self["RESULT_DIR_PATH"])
        return True


    def run(self):
        """ Runs the simulation calling `runSofa`

            .. note:: The parameters file **params.json** is created in the output directory
        """
        if self.checkRun():
            jsonFileName = os.path.join(self.params["RESULT_DIR_PATH"], "params.json")

            if self["OUT_TO_FILE"]:
                if self["STDOUT_PATH"] is None:
                    self["STDOUT_PATH"] = os.path.join(self["RESULT_DIR_PATH"], "stdout.out")
                if self["STDERR_PATH"] is None:
                    self["STDERR_PATH"] = os.path.join(self["RESULT_DIR_PATH"], "stderr.out")
                commandLineOut = " > {out} 2> {err}".format(
                    out = self["STDOUT_PATH"],
                    err = self["STDERR_PATH"],
                )
            else:
                commandLineOut = ""


            string_launched = "{sofa_path} -g {g_mode} -n {it} {scene_path} --argv {argv}{outFiles}".format(
                    sofa_path = self.params["SOFA_PATH"],
                    g_mode = self.params["G_MODE"],
                    it = self.params["N_IT"],
                    scene_path = self.params["SCENE_PATH"],
                    argv = jsonFileName,
                    outFiles = commandLineOut,
            )

            json.dump(self.params, open(jsonFileName, 'w'), indent=4)
            open(os.path.join(self.params["RESULT_DIR_PATH"], "string_launched.txt"),'w').write(string_launched)

            call(string_launched, shell=True)


    def setParams(self, dic):
        """ Merges new parameters to the existing dictionary

            :param dict dic: Dictionary of the new parameters that will be added to the object
        """
        self.params = {**self.params, **dic}


    def addSubDir(self, dirname):
        """ Adds a subdirectory in the results tree

            :param str dirname: Full path of the directory
        """
        if os.path.exists(os.path.dirname(dirname)) and not os.path.exists(dirname):
            os.mkdir(dirname)


    def setResDir(self, i=0):
        """ Automatically finds a name for the output directory based on the
            parameter `RESULT_DIR_PATH` as 'dir_name_i'.

            :param int i: Optional: first number to start the recursion
        """
        dirname = self.params["RESULT_DIR_PATH"]
        if dirname is None:
            raise RuntimeError("Specify a prefix directory for the output with RESULT_DIR_PATH")
        d = "{}_{}".format(dirname, i)
        if not os.path.exists(d):
            os.mkdir(d)
            self.params["RESULT_DIR_PATH"] = d
            return d
        else:
            return self.setResDir(i+1)


    def cleanResDir(self):
        """ Will erase all data contained in `RESULT_DIR_PATH` before running the simulation

            .. warning:: Be carefull, this method erases everything contained in the directory !
        """
        dirname = self.params["RESULT_DIR_PATH"]
        if dirname is None:
            raise RuntimeError("Specify a prefix directory for the output with RESULT_DIR_PATH")
        if os.path.exists(dirname):
            call("rm -r {}".format(os.path.join(dirname)), shell=True)
        os.mkdir(dirname)
