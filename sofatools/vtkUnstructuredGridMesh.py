import os
import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy
import numpy as np


class Const:
    def __init__(self,name,vtkId,nbPoints):
        self.name = name
        self.id = vtkId
        self.nbPoints = nbPoints



class Mesh:
    """ Main class for unstructured mesh wrapping vtk `vtkUnstructuredGrid`

        :param str filename: Filename for the input mesh

        .. csv-table:: Available parameters
            :header: Attribute, Type, Description
            :widths: 20, 10, 70

            `filename`, str, Filename of the mesh
            `nbPoints`, int, Number of points
            `positions`, list, List of points position
            `nbCells`, int, Number of cells
            `cells`, list, List of cells
            `cellsType`, list, List of cells type
            `nbVertices`, int, Number of vertices
            `vertices`, list, List of cells of type vertices
            `nbLines`, int, Number of lines
            `lines`, list, List of cells of type line
            `nbTriangles`, int, Number of triangles
            `triangles`, list, List of cells of type triangle
            `nbTetras`, int, Number of tetrahedron
            `tetras`, list, List of cells of type tetrahedra
            `nbHexas`, int, Number of hexahedron
            `hexas`, list, List of cells of type hexahedron

    """
    # VERTEX, LINE, TRIANGLE, TETRA, HEXA = 1, 3, 5, 10, 12
    # Constants
    VERTEX = Const(name='Vertex', vtkId=1, nbPoints=1)
    LINE = Const(name='Line', vtkId=3, nbPoints=2)
    TRIANGLE = Const(name='Triangle', vtkId=5, nbPoints=3)
    TETRA = Const(name='Tetrahedron', vtkId=10, nbPoints=4)
    HEXA = Const(name='Hexahedron', vtkId=12, nbPoints=8)
    nbPointsInCell = {VERTEX.id:VERTEX.nbPoints, LINE.id:LINE.nbPoints, TRIANGLE.id:TRIANGLE.nbPoints, TETRA.id:TETRA.nbPoints, HEXA.id:HEXA.nbPoints}
    name = {VERTEX.id:VERTEX.name, LINE.id:LINE.name, TRIANGLE.id:TRIANGLE.name, TETRA.id:TETRA.name, HEXA.id:HEXA.name}


    def __init__(self, filename):
        if not os.path.exists(filename):
            raise FileNotFoundError(filename)
        self.filename = filename

        reader = vtk.vtkUnstructuredGridReader()
        reader.SetFileName(filename)
        reader.Update()
        self.output = reader.GetOutput()


    def getCells(self, typ):
        cellsId = []
        if self.output is not None:
            for i in range(self.output.GetNumberOfCells()):
                if self.output.GetCellType(i)==typ:
                    cellsId.append(Cell(data=self.output, cellID=i))
        return cellsId

    ##### Read-Only dynamic attributes
    @property
    def positions(self):
        p = list()
        for i in range(self.output.GetNumberOfPoints()):
            p.append(self.output.GetPoint(i))
        return p
    @property
    def points(self):
        for i in range(self.output.GetNumberOfPoints()):
            yield Point(self.output, i)
    @property
    def cells(self): return Cells(self.output)
    @property
    def nbPoints(self): return self.output.GetNumberOfPoints()
    @property
    def nbCells(self): return self.output.GetNumberOfCells()
    @property
    def vertices(self): return self.getCells(Mesh.VERTEX.id)
    @property
    def lines(self): return self.getCells(Mesh.LINE.id)
    @property
    def triangles(self): return self.getCells(Mesh.TRIANGLE.id)
    @property
    def tetras(self): return self.getCells(Mesh.TETRA.id)
    @property
    def hexas(self): return self.getCells(Mesh.HEXA.id)
    @property
    def nbVertices(self): return len(self.vertices)
    @property
    def nbLines(self): return len(self.lines)
    @property
    def nbTriangles(self): return len(self.triangles)
    @property
    def nbTetras(self): return len(self.tetras)
    @property
    def nbHexas(self): return len(self.hexas)
    @property
    def cellsType(self):
        cT = list()
        for i in range(self.nbCells):
            cT.append(self.output.GetCellType(i))
        return cT

    def __repr__(self):
        """ Provides an easy to read overview of the mesh
        """
        s = "\n   ** Class vtk unstructured grid reader **\n"
        s+= f"Filename : {self.filename}\n"
        s+= f"Points : {self.nbPoints}\n"
        s+= f"Cells : {self.nbCells}\n"
        s+= f"Tetras : {self.nbTetras}\n"
        s+= f"Triangles : {self.nbTriangles}\n"
        s+= f"Vertices : {self.nbVertices}\n" # if self.nbVertices>0:
        s+= f"Lines : {self.nbLines}\n" # if self.nbLines>0:
        s+= f"Hexas : {self.nbHexas}\n" # if self.nbHexas>0:
        return s+"\n"

    def getPointData(self, name):
        """ Accessor to the data array of name `name`

            :param str name: Name of the data array as it is in the vtk file

            :returns: DataArray of the vtk data
        """
        vtkArray = self.output.GetPointData().GetArray(name)
        if vtkArray is None:
            raise AttributeError(f"PointData {name} not found in {self.filename}")
        return DataArray(vtkArray)

    def getCellData(self, name):
        """ Accessor to the data array of name `name`

            :param str name: Name of the data array as it is in the vtk file

            :returns: DataArray of the vtk data
        """
        vtkArray = self.output.GetCellData().GetArray(name)
        if vtkArray is None:
            raise AttributeError(f"CellData {name} not found in {self.filename}")
        return DataArray(vtkArray)

    def addPointData(self, array, name):
        """ Add `array` as FIELD of name `name` to the dataset

            :param ndarray array: Numpy array representing the data to add on the mesh vertices
            :param str name: dataName of the added data
        """
        vtkDataArray = numpy_to_vtk(array)
        vtkDataArray.SetName(name)
        self.output.GetPointData().AddArray(vtkDataArray)

    def addCellData(self, array, name):
        """ Add `array` as FIELD of name `name` to the dataset

            :param ndarray array: Numpy array representing the data to add on the cells of the mesh
            :param str name: dataName of the added data
        """
        vtkDataArray = numpy_to_vtk(array)
        vtkDataArray.SetName(name)
        self.output.GetCellData().AddArray(vtkDataArray)

    def removePointData(self, name):
        """ Remove FIELD of name `name` from the dataset

            :param str name: dataName of the data to remove
        """
        vtkArray = self.output.GetPointData().GetArray(name)
        if vtkArray is None: raise AttributeError(f"PointData {name} not found in {self.filename}")
        else: self.output.GetPointData().RemoveArray(name)

    def removeCellData(self, name):
        """ Remove FIELD of name `name` from the dataset

            :param str name: dataName of the data to remove
        """
        vtkArray = self.output.GetCellData().GetArray(name)
        if vtkArray is None: raise AttributeError(f"CellData {name} not found in {self.filename}")
        else: self.output.GetCellData().RemoveArray(name)

    def write(self, fname):
        """ Write vtk unstructured grid to file `fname`

            :param str fname: Output filename
        """
        writer = vtk.vtkUnstructuredGridWriter()
        writer.SetFileName(fname)
        writer.SetInputData(self.output)
        writer.Write()




class Cells:
    """ Class providing a list of all cells in the mesh

        :param vtkOutput data: vtk output of the vtk internal mesh reader
    """
    def __init__(self, data):
        self.data = data
        self.size = data.GetNumberOfCells()

    def __getitem__(self, i):
        """ Accessor to cell *i*

            :param int i: Number of the cell
        """
        if i==self.size:
            raise StopIteration
        return Cell(self.data, i)

    def __len__(self):
        """ Lenght of the cells list, identical to the number of cells
        """
        return self.size

    def __repr__(self):
        """ Provides an easy to read overview of the cells list
        """
        return  "\n   ** Class Cells **\n" \
                f"Size : {self.size}\n"


class Point:
    def __init__(self, vtkOutput, id):
        self.id = id
        self.position = np.array(vtkOutput.GetPoint(id))

    def __repr__(self):
        """ Provides an easy to read overview of a point
        """
        return  "\n   ** Class Point **\n" \
                f"Id: {self.id}\n" \
                f"Coords: {self.position}\n"



class Cell:
    """ Base class to represent a vtk cell.

        :param vtkCell cell: Cell extracted from the mesh by the internal vtk mesh reader
        :param int cellID: Vtk identifier of the cell

        .. csv-table:: Static attributes
            :header: Attribute, Type, Description
            :widths: 20, 10, 70

            `nbP`, int, Number of points corresponding to the cell type
            `ids`, str, Name of cell corresponding to its type

        .. csv-table:: Attributes
            :header: Attribute, Type, Description
            :widths: 20, 10, 70

            `type`, int, Type of the cell
            `id`, int, Id of the cell in the whole mesh
            `points`, list, List of points id

    """

    def __init__(self, data, cellID):
        self.data = data
        self.cell = data.GetCell(cellID)
        self.type = self.cell.GetCellType()
        self.id = cellID
        # pts = list()
        # for i in range(Mesh.nbPointsInCell[self.type]):
        #     pts.append(Point(self.data, self.cell.GetPointId(i)))
        # self.points = pts

    @property
    def points(self):
        for i in range(Mesh.nbPointsInCell[self.type]):
            yield Point(self.data, self.data.GetCell(self.id).GetPointId(i))

    def __getitem__(self, i):
        """ Accessor to the ith point id of the cell

            :param int i: Number of the point in the cell

            :returns: Point id in the whole mesh
        """
        if not 0<=i<Mesh.nbPointsInCell[self.type]:
            raise IndexError
        return Point(self.data, self.data.GetCell(self.id).GetPointId(i))

    def __len__(self):
        """ Number of points of the cell, corresponding to the cell type
        """
        return Mesh.nbPointsInCell[self.type]

    def __repr__(self):
        """ Provides an easy to read overview of the cell
        """
        return  "\n   ** Class Cell **\n" \
                f"Type : {Mesh.name[self.type]} ({self.type})\n" \
                f"Id : {self.id}\n" \
                f"Points : {' '.join(str(p.id) for p in self.points)}\n"

    def getCenter(self):
        pos = np.zeros(3)
        for p in self.points:
            pos += p.position
        return pos/Mesh.nbPointsInCell[self.type]




class DataArray:
    """ Base class to represent a vtk data array

        :param vtkDataArray vtkArray: Vtk data extracted from the mesh by the intenal vtk mesh reader

        .. csv-table:: Attributes
            :header: Attribute, Type, Description
            :widths: 20, 10, 70

            name, str, Name of the array as it is in the vtk mesh
            range, list(2), Range of the data
            size, int, Number of data contained in the array
            componentSize, int, Number of coordinates each data contains
    """
    def __init__(self, vtkArray):
        self.vtkData = vtkArray
        self.array = vtk_to_numpy(vtkArray)
        self.name = vtkArray.GetName()
        self.range = vtkArray.GetRange()
        self.size = vtkArray.GetNumberOfTuples()
        self.componentSize = vtkArray.GetNumberOfComponents()

    def __getitem__(self, i):
        """ Accessor to the data in the data array

            :param int i: Number of the data in the array

            :returns: The data contained in the array
        """
        # if i==self.size:
        #     raise StopIteration
        # return self.vtkData.GetTuple(i)
        return self.array[i]

    def __str__(self):
        """ Provides an easy to read overview of the data
        """
        s = "\n   ** Class DataArray **\n"
        s += "Name : {} \n".format(self.name)
        s += "Range : {}\n".format(self.range)
        s += "Size : {}\n".format(self.size)
        s += "Component size : {}\n".format(self.componentSize)
        return s

    def __len__(self):
        """ Size of the data array
        """
        return self.size
