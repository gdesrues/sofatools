import unittest
from os.path import join, basename
from sofatools import Mesh
import numpy as np

class TestMesh(unittest.TestCase):
    def setUp(self):
        self.m = Mesh(join("sofatools", "tests", "mesh", "mesh.vtk"))

    def test_dummy_file(self):
        with self.assertRaises(FileNotFoundError):
            Mesh(join("foo.vtk"))

    def test_filename(self):
        self.assertEqual(basename(self.m.filename), 'mesh.vtk')

    def test_const(self):
        self.assertEqual(Mesh.TETRA.id,10)
        self.assertEqual(Mesh.TRIANGLE.name,'Triangle')
        self.assertEqual(Mesh.nbPointsInCell[Mesh.LINE.id],2)

    def test_positions(self):
        self.assertGreater(len(self.m.positions), 0)
        self.assertEqual(self.m.positions[65], (8,.5,0))

    def test_cells(self):
        self.assertEqual(self.m.cells[11][1].id, 152)
        self.assertEqual(len(self.m.cells[11]), 4)
        self.assertEqual(self.m.cells[34][0].id, 23)

    def test_tetras(self):
        self.assertGreater(len(self.m.tetras), 0)
        self.assertEqual(self.m.tetras[34][0].id, 23)

    def test_data_array(self):
        with self.assertRaises(AttributeError):
            self.m.getCellData('Foo')
        cellData = self.m.getCellData('Zones')
        self.assertIsInstance(cellData.array, np.ndarray)

    def test_points(self):
        from sofatools.vtkUnstructuredGridMesh import Point
        point = next(self.m.points)
        self.assertIsInstance(point, Point)
        np.testing.assert_array_equal(point.position, np.array([0,1,0]))


if __name__ == '__main__':
    unittest.main()
