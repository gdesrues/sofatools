SofaTools's package documentation
=====================================

The `sofatools` package was born from the need to launch several scenes with varying
parameters. It is very basic but yet useful.
It includes a reader/writer for vtk unstructured meshes, which allows to quickly
retrieve data from 3D objects, manage DataArrays and write vtu file.
Additionnaly, the affScene module can display the graph of a scene, giving
an overview of complex scenes.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. role:: python(code)
  :language: python


Install
========

Requirements
------------

- vtk

Using pip
------------

.. code:: bash

  pip install sofatools
  # or python3 -m pip install --user sofatools

From sources
------------

.. code:: bash

  git clone git@gitlab.inria.fr:gdesrues1/sofatools.git


Launcher
========

.. autoclass:: sofatools.Launcher
  :members:
  :special-members:
  :member-order: bysource
  :exclude-members: __init__, __weakref__


Mesh
==========

.. autoclass:: sofatools.Mesh
  :members:
  :special-members:
  :member-order: bysource
  :exclude-members: __init__, __weakref__


.. automodule:: sofatools.vtkUnstructuredGridMesh
  :members:
  :special-members:
  :member-order: bysource
  :exclude-members: __init__, __weakref__, Const, Mesh


Graph
=====

.. autoclass:: sofatools.Graph
  :members:
  :special-members:
  :member-order: bysource
  :exclude-members: __init__, __weakref__, __str__
